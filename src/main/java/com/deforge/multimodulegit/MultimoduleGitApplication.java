package com.deforge.multimodulegit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultimoduleGitApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultimoduleGitApplication.class, args);
    }

}
